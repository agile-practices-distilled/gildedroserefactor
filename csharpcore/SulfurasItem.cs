namespace csharpcore
{
    public class SulfurasItem : Item
    {
        private const string SulfurasHandOfRagnaros = "Sulfuras, Hand of Ragnaros";

        public SulfurasItem(int sellIn, int quality): base(SulfurasHandOfRagnaros, sellIn, quality)
        {
        }
        
        public override void Process()
        {
        }
    }
}