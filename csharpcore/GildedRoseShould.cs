﻿using Xunit;
using System.Collections.Generic;

namespace csharpcore
{
    public class GildedRoseShould
    {
        private const string SulfurasHandOfRagnaros = "Sulfuras, Hand of Ragnaros";
        private const string BackstagePassesToATafkal80EtcConcert = "Backstage passes to a TAFKAL80ETC concert";
        private const string AgedBrie = "Aged Brie";
        
        [Fact]
        public void reduceByOneQualityIfQualityAbove0AndNameNotSulfurasOrAgedOrBackStage()
        {
            var item = new Item ("",50,3);
            
            item.Process();
            
            Assert.Equal(2, item.Quality);
        }
        
        [Theory]
        [InlineData(47, 5,50)]
        [InlineData(1, 1,4)]
        [InlineData(33,2,36)]
        public void increaseByThreeQualityIfNameEqualsBackStageSellInBelow6AndQualityBelow48(int quality, int sellin, int result)
        {
            var item = new BackStagePassesItem(sellin, quality );
            
            item.Process();
            
            Assert.Equal(result, item.Quality);
        }
        
        [Theory]
        [InlineData(48, 6,50)]
        [InlineData(1, 8,3)]
        [InlineData(33,10,35)]
        public void increaseByTwoQualityIfNameEqualsBackStageSellInAbove5AndBelow11AndQualityBelow49(int quality, int sellin, int result)
        {            
        var item = new BackStagePassesItem(sellin, quality );
        
            item.Process();
            
            Assert.Equal(result, item.Quality);
        }
        
        [Theory]
        [InlineData(49,48)]
        [InlineData(1,0)]
        [InlineData(33,32)]
        [InlineData(333,332)]
        public void reduceByOneQualityIfNameDifferentToAllAndSellInAbove0AndQualityBelow50(int quality, int result)
        {
            var item = new Item ("", 1, quality);
            
            item.Process();
            
            Assert.Equal(result, item.Quality);
        }
        
        [Fact]
        public void reduceByOneSellInIfNameNotSulfuras()
        {
            var item = new Item();
            
            item.Process();
            
            Assert.Equal(-1, item.SellIn);
        }
        
        [Fact]
        public void reduceByOneQualityIfSellInBelow0AndQualityAbove0AndNameNotSulfurasOrAgedOrBackStage()
        {
            var item = new Item ("",-1,1 );
            
            item.Process();
            
            Assert.Equal(0, item.Quality);
        }
        
        [Theory]
                [InlineData(0,2,0)]
                [InlineData(-3,13,11)]
                [InlineData(-6,52,50)]
        public void reduceByTwoQualityIfQualityAbove1AndSellInBelow1AndNameNotSulfurasOrAgedOrBackStage(int inputSellIn, int inputQuality, int expectedQuality)
        {
            var item = new Item ("",inputSellIn,inputQuality);
            
            item.Process();
            
            Assert.Equal(expectedQuality, item.Quality);
        }
        
        [Fact]
        public void notReduceQualityIfSellInBelow0AndQualityNotAbove0()
        {
            var item = new Item ("",-1,0 );
            
            item.Process();
            
            Assert.Equal(0, item.Quality);
        }
        
        [Theory]
        [InlineData(-10)]
        [InlineData(4)]
        [InlineData(888)]
        public void setQualityToZeroWhenSellInBelow0AndNamedBackStage(int quality)
        {
            var item = new BackStagePassesItem( 0, quality );
            
            item.Process();
            
            Assert.Equal(0, item.Quality);
        }
        
        [Theory]
        [InlineData(22,24)]
        [InlineData(1,3)]
        [InlineData(48,50)]
        public void increaseByTwoQualityIfSellInBelow0AndQualityBelow49AndNameAged(int input, int expectedOutput)
        {
            var item = new AgedBrieItem( -1, input);
            
            item.Process();
            
            Assert.Equal(expectedOutput, item.Quality);
        }
        
        [Fact]
        public void increaseByOneQualityIfSellInBelow0AndQualityExactly49AndNameAged()
        {
            var item = new AgedBrieItem( -1, 49);
            
            item.Process();
            
            Assert.Equal(50, item.Quality);
        }
        
        [Theory]
        [InlineData(50,50)]
        [InlineData(111,111)]
        [InlineData(89,89)]
        public void notIncreaseQualityIfSellInAbove49AndQualityBelow49AndNameAged(int input, int expectedOutput)
        {
            var item = new AgedBrieItem( -1, input);
            
            item.Process();
            
            Assert.Equal(expectedOutput, item.Quality);
        }
    }
}