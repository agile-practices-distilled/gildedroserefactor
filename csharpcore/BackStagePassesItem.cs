namespace csharpcore
{
    public class BackStagePassesItem : Item
    {
        private const string BackstagePassesToATafkal80EtcConcert = "Backstage passes to a TAFKAL80ETC concert";

        public BackStagePassesItem(int sellIn, int quality) :
            base(BackstagePassesToATafkal80EtcConcert, sellIn, quality)
        {
        }

        public override void Process()
        {
            if (Quality < 50)
                Quality++;

            if (Quality < 50)
            {
                if (SellIn < 11)
                    Quality++;

                if (SellIn < 6)
                    Quality++;
            }

            SellIn--;

            if (SellIn < 0)
                Quality = 0;
        }
    }
}