namespace csharpcore
{
    public class AgedBrieItem : Item
    {
        private const string AgedBrie = "Aged Brie";

        public AgedBrieItem(int sellIn, int quality) : base(AgedBrie, sellIn, quality)
        {
        }
        
        public override void Process()
        {
            if (Quality < 50)
                Quality++;
                    
            SellIn--;
                    
            if (SellIn < 0 && Quality < 50)
                Quality++;
        }
    }
}