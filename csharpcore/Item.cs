﻿namespace csharpcore
{
    public class Item
    {
        private string Name { get; }
        public int SellIn { get; protected set; }
        public int Quality { get; protected set; }

        public Item(){}

        public Item(string name, int sellIn, int quality)
        {
            Name = name;
            SellIn = sellIn;
            Quality = quality;
        }

        public virtual void Process()
        {
            if (Quality > 0)
                Quality--;
        
            SellIn--;
            
            if (SellIn < 0 && Quality > 0)
            {
                Quality--;
            }
        }

        public override string ToString()
        {
            return $"{Name}, {SellIn}, {Quality}";
        }
    }
}
