﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ApprovalUtilities.Utilities;

namespace csharpcore
{
    public class Program
    {
        private const int Days = 31;
        
        public static void Main()
        {
            Console.WriteLine("OMGHAI!");

            IList<Item> items = new List<Item>{
                new Item ("+5 Dexterity Vest", 10, 20),
                new AgedBrieItem(2, 0),
                new Item ("Elixir of the Mongoose",5, 7),
                new SulfurasItem(0,80),
                new SulfurasItem(-1,80),
                new BackStagePassesItem(15,20),
                new BackStagePassesItem(10,49),
                new BackStagePassesItem(5,49),
				// this conjured item does not work properly yet
				new Item ("Conjured Mana Cake",3, 6)
            };

            for (var i = 0; i < Days; i++)
            {
                Console.WriteLine("-------- day " + i + " --------");
                Console.WriteLine("name, sellIn, quality");
                items.ForEach(Console.WriteLine);
                Console.WriteLine();
                items.ForEach(item => item.Process());
            }
        }
    }
}
